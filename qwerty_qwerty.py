import evdev.ecodes as evecodes

# key to key mappings
keyByKey = {}

keyByKey = {
    getattr(evecodes, 'KEY_{}'.format(byKey.upper())): getattr(evecodes, 'KEY_{}'.format(key.upper()))
    for byKey, key in keyByKey.items()
}
#

# key to character mappings
qwerty = {
    'a': 'a',
    'b': 'b',
    'c': 'c',
    'd': 'd',
    'e': 'e',
    'f': 'f',
    'g': 'g',
    'h': 'h',
    'i': 'i',
    'j': 'j',
    'k': 'k',
    'l': 'l',
    'm': 'm',
    'n': 'n',
    'o': 'o',
    'p': 'p',
    'q': 'q',
    'r': 'r',
    's': 's',
    't': 't',
    'u': 'u',
    'v': 'v',
    'w': 'w',
    'x': 'x',
    'y': 'y',
    'z': 'z',
    '0': '0',
    '1': '1',
    '2': '2',
    '3': '3',
    '4': '4',
    '5': '5',
    '6': '6',
    '7': '7',
    '8': '8',
    '9': '9',
    'grave': '`',
    'minus': '-',
    'equal': '=',
    'leftbrace': '[',
    'rightbrace': ']',
    'semicolon': ';',
    'apostrophe': '\'',
    'backslash': '\\',
    'comma': ',',
    'dot': '.',
    'slash': '/',
    'tab': '\t',
    'space': ' ',
}

qwertyShift = {
    'grave': '~',
    '1': '!',
    '2': '@',
    '3': '#',
    '4': '$',
    '5': '%',
    '6': '^',
    '7': '&',
    '8': '*',
    '9': '(',
    '0': ')',
    'minus': '_',
    'equal': '+',
    'leftbrace': '{',
    'rightbrace': '}',
    'semicolon': ':',
    'apostrophe': '"',
    'backslash': '|',
    'comma': '<',
    'dot': '>',
    'slash': '?',
}

mapping = qwerty
shiftMapping = qwertyShift

characterByKey = {}
characterByKey.update(
    (getattr(evecodes, 'KEY_{}'.format(key.upper())), character) for key, character in mapping.items())

characterByShiftKey = {}
characterByShiftKey.update(
    (getattr(evecodes, 'KEY_{}'.format(key.upper())), character.upper()) for key, character in mapping.items())
characterByShiftKey.update(
    (getattr(evecodes, 'KEY_{}'.format(key.upper())), character) for key, character in shiftMapping.items())
#
