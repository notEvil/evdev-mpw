**!!! DEPRECATED, check out https://gitlab.com/notEvil/keyboard instead !!!**

# evdev-mpw

## What is it?

**Prototype, use at your own risk!**

A python script which uses evdev and Master Password to capture keyboard events and replace a certain pattern by a password.

Check out https://gitlab.com/MasterPassword/MasterPassword.

## How to?

- pip3 install evdev
- git clone https://gitlab.com/notEvil/evdev-mpw.git
- cd evdev-mpw

The script needs to map keys to characters. Adjust *qwerty_qwerty.py* accordingly.

- python3 evdev_mpw.py --help
- sudo python3 evdev_mpw.py "/dev/input/by-path/..." "/path/to/compiled/Master Password/cli-c/mpw" --init # only once
- sudo python3 evdev_mpw.py "/dev/input/by-path/..." "/path/to/compiled/Master Password/cli-c/mpw"

- in a password form field, type the master password followed by @
- the input (master password) gets replaced by just @
- type the site name followed by Return or Tab
  - if empty: uses `"sudo"`
- the input (@ and site name) gets replaced by password and confirmed by Return or Tab
- you can correct mistakes using Backspace and abort at any time with Esc

or in short

- type `{master password}@{site name}` and confirm with Return or Tab
- correct mistakes with Backspace
- abort with Esc

### Examples

- `{master password}@` for login and sudo
- `{master password}@gitlab.com` for gitlab
