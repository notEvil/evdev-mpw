import evdev
import evdev.ecodes as evecodes
import argparse
import collections
import getpass
import hashlib
import itertools as it
import logging
import os
import stat
import struct
import subprocess as sub
import time

# configuration
import qwerty_qwerty

configuration = qwerty_qwerty

keyByKey = getattr(configuration, 'keyByKey', {})
characterByKey = getattr(configuration, 'characterByKey', {})
characterByShiftKey = getattr(configuration, 'characterByShiftKey', {})

keyByCharacter = {}
keyByCharacter.update((character, key) for key, character in characterByKey.items())

shiftKeyByCharacter = {}
shiftKeyByCharacter.update((character, key) for key, character in characterByShiftKey.items())
#

KEY_UP = 0
KEY_DOWN = 1
KEY_HOLD = 2


def deque_slice(x, s):
    start = s.start
    if start is not None and start < 0:
        start = len(x) + start

        if start < 0:
            start = 0

    stop = s.stop
    if stop is not None and stop < 0:
        stop = len(x) + stop

        if stop < 0:
            stop = 0

    return it.islice(x, start, stop, s.step)


class PassEvents:
    def __init__(self, events, device):
        self.Events = events
        self.Device = device

    def __iter__(self):
        for event in self.Events:
            yield event
            self.Device.write_event(event)


class FilterEvents:
    def __init__(self, events, filter=lambda event: event.type == evecodes.EV_KEY):
        self.Events = events
        self.Filter = filter

    def __iter__(self):
        r = (event for event in self.Events if self.Filter(event))
        return r


class MapKeys:
    def __init__(self, keyEvents, mapping):
        self.KeyEvents = keyEvents
        self.Mapping = mapping

    def __iter__(self):
        for keyEvent in self.KeyEvents:
            keyCode = keyEvent.code
            keyCode = self.Mapping.get(keyCode, None)

            if keyCode is None:
                yield keyEvent
                continue

            n = evdev.InputEvent(keyEvent.sec, keyEvent.usec, keyEvent.type, keyCode, keyEvent.value)
            yield n


class IdentifyModifiers:
    SHIFT_KEYS = [evecodes.KEY_LEFTSHIFT, evecodes.KEY_RIGHTSHIFT]
    CTRL_KEYS = [evecodes.KEY_LEFTCTRL, evecodes.KEY_RIGHTCTRL]

    def __init__(self, keyEvents):
        self.KeyEvents = keyEvents

        self.Shift = False
        self.Ctrl = False
        self.Capslock = False

    def __iter__(self):
        for keyEvent in self.KeyEvents:
            keyCode = keyEvent.code

            if keyCode in IdentifyModifiers.SHIFT_KEYS:
                self.Shift = keyEvent.value != KEY_UP
                continue

            if keyCode in IdentifyModifiers.CTRL_KEYS:
                self.Ctrl = keyEvent.value != KEY_UP
                continue

            if keyCode == evecodes.KEY_CAPSLOCK:
                if keyEvent.value == KEY_DOWN:
                    self.Capslock = not self.Capslock
                continue

            yield keyEvent


class IdentifyCharacters:
    def __init__(self, keyEvents, mapping, modifiers, shiftMapping):
        self.KeyEvents = keyEvents
        self.Mapping = mapping
        self.Modifiers = modifiers
        self.ShiftMapping = shiftMapping

        self.Input = collections.deque(maxlen=32)

    def __iter__(self):
        for keyEvent in self.KeyEvents:
            if keyEvent.value == KEY_UP or self.Modifiers.Ctrl:
                continue

            if keyEvent.code == evecodes.KEY_BACKSPACE:
                if len(self.Input) != 0:
                    self.Input.pop()

            else:
                m = self.ShiftMapping if self.Modifiers.Shift or self.Modifiers.Capslock else self.Mapping
                character = m.get(keyEvent.code, ' ')
                self.Input.append(character)

            yield keyEvent


class ReplacePassword:
    def __init__(self, keyEvents, characters, passwordLen, passwordHash, keyByCharacter, shiftKeyByCharacter, device):
        self.KeyEvents = keyEvents
        self.Characters = characters
        self.PasswordLen = passwordLen
        self.PasswordHash = passwordHash
        self.KeyByCharacter = keyByCharacter
        self.ShiftKeyByCharacter = shiftKeyByCharacter
        self.Device = device

    def __iter__(self):
        keyEvents = iter(self.KeyEvents)

        for keyEvent in keyEvents:
            # react on @
            if keyEvent.code == evecodes.KEY_BACKSPACE or len(
                    self.Characters.Input) == 0 or self.Characters.Input[-1] != '@':
                continue

            input = ''.join(self.Characters.Input)

            pw = input[(-1 - self.PasswordLen):-1]

            blake = hashlib.blake2b()
            blake.update(pw.encode())
            hash = blake.digest()

            if hash != self.PasswordHash:
                continue

            logging.info('input hash matches password hash')

            for _ in range(self.PasswordLen):  # undo input
                self._write(evecodes.KEY_BACKSPACE, KEY_DOWN)
                self._write(evecodes.KEY_BACKSPACE, KEY_UP)

            # get site
            for keyEvent in keyEvents:
                if keyEvent.code in [evecodes.KEY_ENTER, evecodes.KEY_ESC, evecodes.KEY_TAB]:
                    break

            if keyEvent.code == evecodes.KEY_ESC:
                logging.info('cancelled')
                del pw
                continue

            input = ''.join(self.Characters.Input)

            i = input.rfind(pw + '@')

            if i == -1:
                del pw
                continue

            site = input[(i + self.PasswordLen + 1):-1]
            #

            for _ in range(1 + len(site)):  # undo input
                self._write(evecodes.KEY_BACKSPACE, KEY_DOWN)
                self._write(evecodes.KEY_BACKSPACE, KEY_UP)

            if site == '':
                site = 'sudo'

            # get password
            logging.info('generating password for site {}'.format(repr(site)))

            aa = [
                'runuser', '-u', 'arappold', '--', args.mpw, '-u', 'Andreas Rappold', '-m', '0', '-P', '-p', site,
                '-c', args.count
            ]
            process = sub.Popen(aa, stdin=sub.PIPE, stdout=sub.PIPE, stderr=sub.DEVNULL)

            pw, _ = process.communicate(input=pw.encode())
            #

            pw = pw.decode()
            pw = pw.strip()

            for character in pw:
                keyCode = self.KeyByCharacter.get(character, None)
                if keyCode is not None:
                    self._write(keyCode, KEY_DOWN)
                    self._write(keyCode, KEY_UP)
                    continue

                keyCode = self.ShiftKeyByCharacter[character]
                self._write(evecodes.KEY_LEFTSHIFT, KEY_DOWN)
                self._write(keyCode, KEY_DOWN)
                self._write(keyCode, KEY_UP)
                self._write(evecodes.KEY_LEFTSHIFT, KEY_UP)

            del pw

            logging.info('input replaced by password')

            yield keyEvent

    def _write(self, code, value):
        self.Device.write(evecodes.EV_KEY, code, value)


# parse arguments
parser = argparse.ArgumentParser()

parser.add_argument('dev', help='Path to input device')
parser.add_argument('mpw', help='Path to cli tool of master password')
parser.add_argument('-p', '--password', default='./password', help='Path to password file (default: ./password)')
parser.add_argument('-w', '--wait', default='1', help='Number of seconds to wait before capturing input (default: 1)')
parser.add_argument('-c', '--count', default='1', help='Count for master password (default: 1)')
parser.add_argument('-l', '--log', default=None, help='Path to log file (default: stderr)')
parser.add_argument('-v', '--verbose', action='store_true', default=False, help='Be verbose')
parser.add_argument('-d', '--debug', action='store_true', default=False, help='Debug mode')
parser.add_argument('--init', action='store_true', default=False, help='Initialize')

args = parser.parse_args()
#

# setup logging
if args.debug:
    args.verbose = True

if args.verbose:
    kwargs = dict(level='DEBUG' if args.debug else 'INFO')
    if args.log is not None:
        kwargs['filename'] = args.log

    logging.basicConfig(**kwargs)
#

if args.init:
    # get password hash
    pw = getpass.getpass()

    pwLen = len(pw)

    blake = hashlib.blake2b()
    blake.update(pw.encode())
    pwHash = blake.digest()

    del pw
    #

    path = args.password

    open(path, 'wb').close()
    uid = os.getuid()
    os.chown(path, uid, uid)
    os.chmod(path, stat.S_IWUSR)

    with open(path, 'wb') as f:
        f.write(struct.pack('B', pwLen))
        f.write(pwHash)

    os.chmod(path, stat.S_IRUSR)

    exit()

else:
    with open(args.password, 'rb') as f:
        pwLen, = struct.unpack('B', f.read(1))
        pwHash = f.read()

device = evdev.InputDevice(args.dev)

wait = int(args.wait)
time.sleep(wait)

with device.grab_context(), evdev.UInput() as ui:
    events = device.read_loop()
    events = PassEvents(events, ui)
    keyEvents = FilterEvents(events)
    keyEvents = MapKeys(keyEvents, keyByKey)
    modifiers = IdentifyModifiers(keyEvents)
    characters = IdentifyCharacters(modifiers, characterByKey, modifiers, characterByShiftKey)
    passwordReplaces = ReplacePassword(characters, characters, pwLen, pwHash, keyByCharacter, shiftKeyByCharacter, ui)

    for _ in passwordReplaces:
        pass
